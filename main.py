import GraphTheory.BaseGraph as BaseGraph
import GraphTheory.Djikstra as Djikstra
import random
import Algorithms.Sorting as Sorting
import timeit


def algorithms():
    print("Bubble Sort: {}".format(Sorting.get_test_array()))
    print(Sorting.bubble_sort(Sorting.get_test_array()))
    sort_average_time = timeit.Timer('import Algorithms.Sorting as Sorting\nSorting.bubble_sort(Sorting.get_test_array())').repeat(number=5)
    print("Average bubble sort time = {}".format(sort_average_time))

    print("Selection Sort: {}".format(Sorting.get_test_array()))
    print(Sorting.selection_sort(Sorting.get_test_array()))
    sort_average_time = timeit.Timer('import Algorithms.Sorting as Sorting\nSorting.selection_sort(Sorting.get_test_array())').repeat(number=5)
    print("Average selection sort time = {}".format(sort_average_time))

    print("Selection Sort: {}".format(Sorting.get_test_array()))
    print(Sorting.insertion_sort(Sorting.get_test_array()))
    sort_average_time = timeit.Timer('import Algorithms.Sorting as Sorting\nSorting.insertion_sort(Sorting.get_test_array())').repeat(number=5)
    print("Average insertion sort time = {}".format(sort_average_time))

    print("Merge Sort: {}".format(Sorting.get_test_array()))
    print(Sorting.merge_sort(Sorting.get_test_array()))
    sort_average_time = timeit.Timer('import Algorithms.Sorting as Sorting\nSorting.merge_sort(Sorting.get_test_array())').repeat(number=5)
    print("Average merge sort time = {}".format(sort_average_time))

    print("Quick Sort: {}".format(Sorting.get_test_array()))
    print(Sorting.quick_sort(Sorting.get_test_array()))
    sort_average_time = timeit.Timer('import Algorithms.Sorting as Sorting\nSorting.quick_sort(Sorting.get_test_array())').repeat(number=5)
    print("Average quick sort time = {}".format(sort_average_time))


def graph_theory():
    d = Djikstra.DjikstraAlgorithm()
    v1, v2 = random.sample(set(d.graph.get_vertices()), 2)
    print(d.get_distance_between_vertices(vertex_1=v1, vertex_2=v2))


if __name__ == '__main__':
    algorithms()
