import GraphTheory.BaseGraph as BaseGraph


class DjikstraAlgorithm:
    def __init__(self, graph=BaseGraph.get_random_graph(num_vertices=100, num_edges=1000)):
        self.graph = graph
        self.solved_vertices = {k: {} for k in self.graph.vert_dict.keys()}

    def solve(self, starting_node):
        visited_vertices = {k: False for k in self.graph.vert_dict.keys()}
        vertex_distances = {k: 0 if k == starting_node else -1 for k in self.graph.vert_dict.keys()}

        current_node = self.graph.get_vertex(starting_node)

        while not visited_vertices[current_node.get_id()]:
            visited_vertices[current_node.get_id()] = True
            for neighbor_vertex in current_node.get_connections():
                neighbor_vertex_distance = vertex_distances[neighbor_vertex.get_id()]
                neighbor_vertex_distance_to_starting_node = neighbor_vertex.get_weight(current_node) + vertex_distances[current_node.get_id()]

                if neighbor_vertex_distance_to_starting_node < neighbor_vertex_distance or neighbor_vertex_distance == -1:
                    vertex_distances[neighbor_vertex.get_id()] = neighbor_vertex_distance_to_starting_node
            d = {k: v for k, v in vertex_distances.items() if k in [x.get_id() for x in current_node.get_connections()] and not visited_vertices[k]}

            if d:
                current_node = self.graph.get_vertex(min(d, key=d.get))

        self.solved_vertices[starting_node] = vertex_distances

    def print_shortest_path(self, vertex_1, vertex_2):
        if not self.vertex_in_graph(vertex_1) or not self.vertex_in_graph(vertex_2):
            raise Exception("Vertex not in graph")

        if not self.solved_vertices[vertex_1]:
            self.solve(starting_node=vertex_1)

        vertex_distances = self.solved_vertices[vertex_1]
        if vertex_distances[vertex_2] == -1:
            return

        current_node = vertex_1
        shortest_path = []

        while current_node:
            shortest_path += [current_node]
            if current_node.get_id() == vertex_2:
                break
            neighbor_distances = {k: v for k, v in vertex_distances if k in [x.get_id() for x in current_node.get_connections()]}
            current_node = min(neighbor_distances)

    def vertex_in_graph(self, vertex):
        return vertex in self.graph.vert_dict.keys()

    def get_distance_between_vertices(self, vertex_1, vertex_2):
        if not self.vertex_in_graph(vertex_1) or not self.vertex_in_graph(vertex_2):
            raise Exception("Vertex not in graph")

        if not self.solved_vertices[vertex_1]:
            self.solve(starting_node=vertex_1)

        return self.solved_vertices[vertex_1][vertex_2]
