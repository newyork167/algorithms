import math
import numpy as np


def get_test_array():
    np.random.seed(1)
    return list(np.random.randint(100, size=100))


def bubble_sort(arr):
    x = 0
    swapped = True

    while swapped:
        swapped = False
        for i in range(x, len(arr)):
            if arr[x] > arr[i]:
                arr[x], arr[i] = arr[i], arr[x]
                swapped = True
        x += 1
    return arr


def selection_sort(arr):
    sorted_arr = []

    for i in range(len(arr)):
        min_element = min(arr)
        sorted_arr += [min_element]
        arr.pop(arr.index(min_element))

    return sorted_arr


def insertion_sort(arr):
    for i in range(len(arr)):
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[i]:
                arr[j], arr[i] = arr[i], arr[j]
    return arr


def merge_sort(arr):
    if len(arr) <= 1:
        return arr

    left = merge_sort(arr[math.floor(len(arr)/2):])
    right = merge_sort(arr[:math.floor(len(arr)/2)])
    sorted_arr = []

    left_cursor, right_cursor = 0, 0
    while True:
        if left_cursor >= len(left):
            sorted_arr += right[right_cursor:]
            break
        if right_cursor >= len(right):
            sorted_arr += left[left_cursor:]
            break

        if left[left_cursor] <= right[right_cursor]:
            sorted_arr += [left[left_cursor]]
            left_cursor += 1
        else:
            sorted_arr += [right[right_cursor]]
            right_cursor += 1

    return sorted_arr


def quick_sort(arr):
    if not len(arr):
        return arr

    smaller = []
    pivot_arr = []
    bigger = []
    pivot = arr[math.floor(len(arr) / 2)]

    for i in range(len(arr)):
        if arr[i] == pivot:
            pivot_arr += [arr[i]]
        elif arr[i] > pivot:
            bigger += [arr[i]]
        else:
            smaller += [arr[i]]

    smaller_sorted = quick_sort(smaller)
    bigger_sorted = quick_sort(bigger)

    return smaller_sorted + pivot_arr + bigger_sorted


if __name__ == '__main__':
    a = [6, 5, 3, 1, 8, 7, 2, 4]
    print("Bubble Sort: {}".format(a))
    print(bubble_sort(a))
